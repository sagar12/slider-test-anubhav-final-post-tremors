// Your code here.
(function( $ ) {
	var methods = {
		ChangeCarouselImage: function(field) {
			field.siblings().click( function() {
				var currentItem = field.find('li.current');
				if($(this).hasClass("carousel-control-prev")) {
					if(typeof(currentItem.prev()) != 'undefined' && currentItem.prev().length > 0) {
						currentItem.removeClass('current');
						currentItem.prev().addClass('current');
					}
				} else {
					if(typeof(currentItem.next()) != 'undefined' && currentItem.next().length > 0) {
						currentItem.removeClass('current');
						currentItem.next().addClass('current');
					}
				}
			});
		},
		AutoRotateImages: function(field) {
			var carouselItems = field.find('li');
			var currentItem = field.find('li.current');
			$.each(carouselItems, function(index, value) {
				var currentItemIndex;
				if($(this).hasClass('current')) {
					currentItemIndex = index;
					if(index < carouselItems.length - 1) {
						currentItem.removeClass('current');
						currentItem.next().addClass('current');
						return false;
					} else {
						currentItem.removeClass('current');
						carouselItems.first().addClass('current');
					}
				}				
			});
		}
	};
	$.fn.carousel = function( options ) {
		var settings = $.extend({
            // These are the defaults.
            auto: 'false',
            timer: '3000',
            vertical: 'false'
        }, options );
		
		var carousel = this;
		var carouselItems = carousel.find('li');		
		this.find('ul').addClass('unorderedlist');
		carouselItems.addClass('carousel-item');
		carouselItems.first().addClass('current');

		if(settings.auto == 'false'){
			methods.ChangeCarouselImage(carousel);			
		}
		else {
			this.siblings().hide();
			setInterval(function() { 
				methods.AutoRotateImages(carousel); 
			}, settings.timer);
		}
	};
})( jQuery );